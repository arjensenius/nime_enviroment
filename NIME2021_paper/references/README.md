Meta-reviews of NIME proceedings: 

- Jensenius 2014
- Sullivan & Wanderley 2018

Environmental issues in NIME and beyond: 

- Freed 2012: The Fingerphone: a Case Study of Sustainable Instrument Redesign
- Collins 2020 (WIP)
- Dourish 2010: HCI and Environmental Sustainability: The Politics of Design and Design of Politics

