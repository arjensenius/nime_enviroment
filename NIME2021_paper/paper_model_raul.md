<table>
    <thead>
        <tr>
            <th colspan=2 rowspan=2 >  </th>
            <th colspan=5 > Activity </th>
        </tr>
        <tr>
            <th>Communication</th>
            <th>Archiving</th>
            <th> Instrument Design/Development</th>
            <th>User Studies</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=7>Components</td>
            <td>Data Storage</td>
            <td>X</td>
            <td>X</td>
            <td></td>
            <td>X</td>
        </tr>
        <tr>
            <td>Communication Tool</td>
            <td>X</td>
            <td>X</td>
            <td></td>
            <td>X</td>
        </tr>
        <tr>
             <td>Computational Cost</td>
            <td>X</td>
            <td>X</td>
            <td>X</td>
            <td>X</td>
        </tr>
        <tr>
             <td>Material Production</td>
            <td></td>
            <td></td>
            <td>X</td>
            <td></td>
        </tr>
        <tr>
             <td>Material Work/Manipulation</td>
            <td></td>
            <td></td>
            <td>X</td>
            <td></td>
        </tr>
        <tr>
             <td>Material Lifetime</td>
            <td></td>
            <td></td>
            <td>X</td>
            <td></td>
        </tr>
        <tr>
             <td>Design for Reuse</td>
            <td></td>
            <td></td>
            <td>X</td>
            <td></td>
        </tr>
    </tbody>
</table>

X = most likely to have impac (my personal extimation)

- data storage = cost of storing data (server maintenance, energy,  ...) 
- communication tool = cost of the system we use to communicate (maybe this is included in data storage + computational cost)
- computational cost = cost of the computing, especially if machine learning in cloud
- material production = cost to produce the material we use
- material work/manipulation = cost to work the material (chemical reaction, non-sustainable glues, very complex tools?)
- material lifetime = how durable is our material in the instrument or reusable if it is just a prototype
- design for reuse = how easy to fix or easy to use the component for other projects in case of prototypes


