Sustainability issues should be addressed in making as well as writing	

**Design questions**
- Which materials are involved and how are they sourced?    
- Do such materials raise specific 	
- Are materials recyclable or could they be rendered recyclable?	
- What is the energy impact of driving the project, is heavy computation involved and is there a way of reducing this?		
- Does the interface encourage repeated use and exploration?

**Suggestions for future research / papers**
- Computational efficiency is energy efficiency: an overview of the energy impact of digital filter designs
- The energy impact of Machine Learning and Genetic Algorithms
- Sustainable embedded solutions: an overview of the environmental impact of different microprocessors and their computational efficiency 
    versus laptop solutions
- An approach to creating modular and multi-purpose building block DMIs for reuse and recycling
- DMIs that sonifiy their own energy consumption
- DMIs that harvest energy from their own operation (Recycling vibrational energy or using the airflow from speakers to drive mini wind wills)



Examples of materials / data | Source / Production | Energy impact of use | Longevity | Repair | Disposal issues | Harvest energy from use?
--- | --- | --- | --- | --- | --- | --- | 
**Speakers / transducers** |  | | | | | Possibly yes
**Microprocessor** | Several | Low | |Difficult |Yes | Unlikely
**Laptop** | Several | Medium | | Yes |Yes | Unlikely
**Machine Learning** |AWS / Google Cloud / Self-hosted | Possibly high | | | | Unlikely


