Here are some notes about how we could present a framework or matrix of addressing environmental sustainability in NIME. 

Dimensions of sustainable NIME research: 

- research activity: 
    - scientific research
    - artistic practice
    - dissemination
- tools and methods: 
    - hardware
    - software
    - digital services/platforms (might also be software)
- who is involved:
    - individual/personal
    - participants
    - other artists/collaborators
    - NIME colleagues & broader academic community
    - audience/fans
- sustainable practices
    - practicing energy conservation
    - utilizing renewable energy sources
    - reducing material consumption
    - reducing waste
    - recycling
    - raising environmental awareness (via creative works)
    - not traveling
    - purchasing carbon offsets
- location/proximity to researcher
    - local/individual (home or similar)
    - local (physical) community
    - regional
    - global
    - online/virtual

...undoubtedly there are plenty of other dimensions as well, and towards a simple model not all dimensions are compelling. But perhaps this is a start towards characterizing "what" environmentally responsible NIME practice might be comprised of. 

-----

Some ways of visualizing dimensions... Another way would be a "sustainability spider plot" (2-dimensional plot for representing each dimensional on a separate axis), though this would need some work to make something that was actually useful and coherent!  

| (research activity) | Scientific research | Artistic practice | Dissemination | 
| - | - | - | - |
| Hardware | 
| Software | 
| Digital services | 

| **(sustainable practices)** | **Scientific research** | **Artistic practice** | **Dissemination** | 
| - | - | - | - |
| practicing energy conservation |
| utilizing renewable energy sources |
| reducing material consumption |
| reducing waste |
| recycling |
| raising environmental awareness (via creative works) |
| not traveling |
| purchasing carbon offsets |
