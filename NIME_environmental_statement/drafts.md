*This was the current working draft of the statement (12-Oct-2020) that was put to the [forum](forum.nime.org) for comment.*

## NIME Environmental Statement

Every action we perform, including research, has an impact on our ecosystem. We acknowledge that we live in a time of unprecedented environmental crisis caused by human activity and the intensified use of fossil fuel technologies. NIME is committed to lessening the environmental impact of our field through a continuous evaluation of our infrastructure, tools and consumption. 

As the annual conference is the cornerstone of the NIME community, we believe environmental sustainability and conservation should be considered in its planning and execution, and priority given to venues and suppliers that demonstrate commitment to these areas. Beyond the conference, we advocate research and artistic practices that support these same principles of sustainability and conservation.

Specifically, we wish to address the following topics:
- Work with steering committee and conference organisers to create a more sustainable NIME conference
- Promote environmental awareness in our use of tools, technologies, and materials by providing information and resources to researchers [[see below](#eco_nime-repository)]   
- Work with the ethics and diversity committees to ensure that sustainability measures observe and are inclusive of varying geographical, social and economic realities
- Encourage conference submissions that address these above issues

As humans, we have a responsibility towards our environment and ecosystem.

As artists and researchers, we have the ability to give voice to climate change and to confront and question damaging privileges and habits, through the development of new practices, technologies and infrastructures that lessen our planetary impact.

As individuals, our impact is negligible but as a community, we can make a difference.

-----

> ### ECO_NIME repository
>
> **Info and resources on environmental issues in NIME**
>
> To support sustainable practices within and beyond NIME, we have created a repository containing information and green resources for environmental issues in NIME research. This repository will be continuously updated and contain information, ideas, and suggestions for how we can lessen our individual as well as institutional footprint. It is softly curated by the environmental officers, and we highly encourage everyone to submit requests for additions and edits. We hope that this will become a resource, not only for evaluating our environmental impact but also spawn new ideas, designs, sonic works, and collaborations, possibly having an impact also beyond the boundaries of NIME research. [We will add here a the link to the repository when this statement will be published on the NIME website]

----------

# The statement

Every action we perform, including research, has an impact on our ecosystem. We acknowledge that we live in a time of unprecedented environmental crisis caused by human activity and the intensified use of fossil-fuel technologies. 
NIME is committed to lessening our environmental impact through a continuous evaluation of our infrastructure, tools and consumption. As the centerpiece of NIME is the annual conference, we believe environmental sustainability and conservation should be considered in its planning and execution, and priority given to venues and suppliers that demonstrate commitment to these areas. 
As humans, we have a responsibility towards our environment and ecosystem. As artists and researchers, we have the ability to give voice to climate change and to confront and question damaging privileges and habits, through the development of new practices, technologies and infrastructures that lessen our planetary impact.
As individuals, our impact is negligible but as a community of researchers and artists, we can make a difference.

Specifically, we wish to address the following topics:
- Work with steering committee and conference organisers to create a more sustainable NIME conference
- Promote environmental awareness in our use of technologies, for example through energy conservation and the use of biodegradable and recyclable materials
- Work with the ethics and diversity committees to ensure that sustainability measures observe and are inclusive of varying geographical, social and economic realities
- Encourage conference submissions that address the above issues

Towards these goals, we developed a repository of information and green resources for environmental issues in NIME research. This repository will be continuously updated and contain information, ideas, and suggestions for how we can lessen our individual as well as institutional footprint. It is softly curated by the environmental officers, and we highly encourage everyone to submit requests for additions and edits. We hope that this will become a resource, not only for evaluating our environmental impact but also spawn new ideas, designs, sonic works, and collaborations, possibly having an impact also beyond the boundaries of NIME research. [We will add here a the link to the repository when this statement will be published on the NIME website]

-------------

## Johnny suggested revision (Wed. am 7-Oct )

## NIME Environmental Statement

Every action we perform, including research, has an impact on our ecosystem. We acknowledge that we live in a time of unprecedented environmental crisis caused by human activity and the intensified use of fossil fuel technologies. NIME is committed to lessening the environmental impact of our field through a continuous evaluation of our infrastructure, tools and consumption. 

As the annual conference is the cornerstone of the NIME community, we believe environmental sustainability and conservation should be considered in its planning and execution, and priority given to venues and suppliers that demonstrate commitment to these areas. Beyond the conference, we advocate research and artistic practices that support these same principles of sustainability and conservation.

Specifically, the NIME environmental officers are committed to the following objectives: 

- Working with the steering committee and organizers to reduce the carbon footprint of NIME conferences related to air travel and physical infrastructures, proposing alternate forms of conference delivery and providing green alternatives for digital conference tools 
- Promoting environmental awareness in our use of tools, technologies, and materials by providing information and resources to researchers [[see below](#eco_nime-repository)]   
- Working with the ethics and diversity committees to ensure that sustainability measures observe and are inclusive of varying geographical, social and economic realities
- Encouraging NIME research and conference submissions that address these above issues

As humans, we have a responsibility towards our environment and ecosystem.

As artists and researchers, we have the ability to give voice to climate change and to confront and question damaging privileges and habits, through the development of new practices, technologies and infrastructures that lessen our planetary impact.

As individuals, our impact is negligible but as a community, we can make a difference.

-----

> ### ECO_NIME repository
>
> **Info and resources on environmental issues in NIME**
>
> To support sustainable practices within and beyond NIME, we have created a repository containing information and green resources for environmental issues in NIME research. This repository will be continuously updated and contain information, ideas, and suggestions for how we can lessen our individual as well as institutional footprint. It is softly curated by the environmental officers, and we highly encourage everyone to submit requests for additions and edits. We hope that this will become a resource, not only for evaluating our environmental impact but also spawn new ideas, designs, sonic works, and collaborations, possibly having an impact also beyond the boundaries of NIME research. [We will add here a the link to the repository when this statement will be published on the NIME website]

------------



## Raul: 

Our goal is to create a more environmentally aware conference. To this end, we are primarily working on 3 directions, 1) supporting NIMErs with information about environmental impacts of technology, 2) organize a more sustainable NIME, and 3) promoting environment-related submission at NIME.

NIME can promote green practices also outside the context of the conference. Every action we perform, including research, has an impact on our ecosystem. Here we try to promote a sustainable approach that we hope can be scale in general to digital technology-related research and maybe everyday life in general. (maybe the last part is too much)

As environmental officers, we want to support green practices. To this end, we prepared a wiki with information about technology's impact on the environment and suggested, when possible green alternatives.
This wiki will be a living document that we will keep to update. If any of you want to contribute, send us an email (if we use the GitLab, maybe you can put my email here, but in any case, I would keep the plural, to give the sense that we operate as a committee).  We will grant you access to contribute in the uncurated contribution folder, we will constantly include the new contribution in the curated part of the wiki.  (this last sentence refects what we said, but feel free to change it)


-----

## Johnny

- I like the [NIME Diversity statement](https://www.nime.org/diversity/) that has both a short and long version. We could do the same... or not.. :) 
- Also I really like Raul's suggestion for the opening to be something like "Every action we perform, including research, has an impact on our ecosystem..."

I think that's a great way to frame the statement and intent. In terms of content, I can suggest the following: 

1. We believe that considering the environmental costs of all aspects of NIME research is imperative, and as researchers, artists, academics, and a community we should be proactive in considering methods and tools that minimize harm to the earth and our ecosystem. 

This may include: 
    - practicing energy conservation wherever possible
    - choosing biodegradable materials whenever possible
    - researching and evaluating supply chains, and supporting sustainable sources
    - weighing impacts of research-related travel, and choosing environmentally friendly modes of transportation where feasible
    - considering energy costs and green options for computing and digital tools/services like machine learning tasks, digital streaming, web hosting
    - (...this is not a very eloquent list but it could be good to list out several specific aspects of environmental concern...)

(then introduce the resource page for researchers... )

2. As the centerpiece of NIME is the annual conference, we believe environmental sustainability and conservation should be considered in its planning and execution, and priority given to venues and suppliers that demonstrate commitment to these areas as well. 

3. Finally, we advocate for NIME research and artworks that illuminate environmental issues, either through their examination and investigation of innovative environmental technologies and practices, or by calling attention to areas, issues, or crises that are in peril.  

^^^ (reading back most of this sounds a bit overbearing and probably long-winded too. feel free to use any/none of it as suits... )

### Johnny again (Mon 28-Sept)

The statement so far is really good! 

2 further thoughts: 

1. Looking at the [NIME cookbook page](https://github.com/NIME-conference/cookbook/blob/master/Officers/environmental.md) the main objectives are directly related to the conference (environmentally friendly physical conference and alternate conference organization). I personally find it equally (or more) important to address bigger picture environmental issues in NIME research and practice, however this may be less the intent of the steering committee, in which case we should ensure that we emphasize our role in conference planning and management.

2. For the statement, at least until we have a clear idea of the format and location of the info and resources wiki, perhaps best to mention it in fairly general terms... "a community-driven repository of information and green resources for environmental issues in NIME research" ...or something.



